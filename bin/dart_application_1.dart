import 'package:dart_application_1/dart_application_1.dart' as dart_application_1;
import 'dart:io' show stdin;

void main() {
  print(
      'MENU\nSelect the choice you want to perform :\n1.ADD\n2.SUBTRACT\n3.MULTIPLY\n4.DIVIDE\n5.EXIT\nChoice you want to enter :\n');
  int choice = int.parse(stdin.readLineSync()!);
  switch (choice) {
    case 1:
      {
        print('Enter the value for x :\n');
        int x = int.parse(stdin.readLineSync()!);
        print('\n');
        print('Enter the value for y :\n');
        int y = int.parse(stdin.readLineSync()!);
        print('\n');
        print('Sum of the two number is :\n');
        print(x + y);
        break;
      }
    case 2:
      {
        print('Enter the value for x :\n');
        int x = int.parse(stdin.readLineSync()!);
        print('\n');
        print('Enter the value for y :\n');
        int y = int.parse(stdin.readLineSync()!);
        print('\n');
        print('Subtraction results of the two number is :\n');
        print(x - y);
        break;
      }
    case 3:
      {
        print('Enter the value for x :\n');
        int x = int.parse(stdin.readLineSync()!);
        print('\n');
        print('Enter the value for y :\n');
        int y = int.parse(stdin.readLineSync()!);
        print('\n');
        print('Multiply results of the two number is :\n');
        print(x * y);
        break;
      }
    case 4:
      {
        print('Enter the value for x :\n');
        int x = int.parse(stdin.readLineSync()!);
        print('\n');
        print('Enter the value for y :\n');
        int y = int.parse(stdin.readLineSync()!);
        print('\n');
        print('Divide results of the two number is :\n');
        print(x / y);
        break;
      }
    case 5:
      {
        print('Exit\n');
        break;
      }
  }
}
